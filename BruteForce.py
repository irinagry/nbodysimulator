#!/usr/bin/python

from math import *
from visual import *
from numpy import *
from os import sys
import time
from random import uniform
from Body import Body

N = 100;
bodies = [];
for i in xrange(N):
    x = Body()
    bodies.append(x)
#print bodies[2]
#print bodies[3]
#print Body.SM;

def signum(x):
    if x == 0:
        return 0.0;
    elif x > 0:
        return 1.0;
    else: 
        return -1.0;

def circlev(rx, ry):
    r2 = math.sqrt(rx*rx + ry*ry);
    numerator = (Body.G)*1e6*Body.SM;
    return math.sqrt(numerator/r2);

def startTheBodies():
    global N;
    R = 1e18; # Radius of Universe

    for i in range(1, N):
        # Initial Positions
        px = R * math.exp(-1.8) * (0.5 - random.uniform(0,1));
        py = R * math.exp(-1.8) * (0.5 - random.uniform(0,1));
        magv = circlev(px, py);

        absangle = math.atan(abs(px/py));
        thetav = math.pi/2 - absangle;
        phiv = random.uniform(0,1) * math.pi;
        vx = -1 * signum(py) * math.cos(thetav) * magv;
        vy = signum(px) * math.sin(thetav) * magv;

        if random.uniform(0,1) <= 0.5:
            vx = -vx;
            vy = -vy;

        mass = random.uniform(0,1) * Body.SM * 10 + 1e20;

        # Color the masses:
        red   = (mass * 254 / (Body.SM * 10 + 1e20)) / 255.0;
        blue  = (mass * 254 / (Body.SM * 10 + 1e20)) / 255.0;
        green = 255 / 255.0;
        #colorm = color.rgb_to_hsv((red, green, blue))
        colorm = (red, green, blue);
        #sphere(radius = 2, color = colorm)

        bodies[i] = Body(px, py, vx, vy, mass, colorm);

    # Central mass in:
    colorm = (1, 0, 0);
    bodies[0] = Body(0, 0, 0, 0, 1e6*Body.SM, colorm);




scene.scale = (.1,.1,.1)

startTheBodies()
for i in range(0, N):
    x = (bodies[i].rx)/(1e16);
    y = (bodies[i].ry)/(1e16);
    z = 0.0
    colorm = bodies[i].color
    print str(x) + ", " + str(y)
    if i == 0:
        sphere(radius = 0.5, pos = [x,y,z], color = colorm)
    else:
        sphere(radius = 0.1, pos = [x,y,z], color = colorm)


