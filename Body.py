#!/usr/bin/python

from visual import *
from math import *
from Quad import Quad

class Body:
    G  = 6.673e-11;
    SM = 1.98892e30;

    # Constructor
    def __init__(self, rx = 0.0, ry = 0.0, vx = 0.0, vy = 0.0, mass = 1.0, color = (0,0,1), radius = 0.1):
        self.rx = rx;
        self.ry = ry;
        self.vx = vx;
        self.vy = vy;
        self.fx = 0;
        self.fy = 0;
        self.mass = mass;
        self.color = color;
        self.radius = radius;

    # Update velocity and position using a timestamp
    def update(self, dt):
        self.vx += dt * self.fx / self.mass;
        self.vy += dt * self.fy / self.mass;
        self.rx += dt * self.vx;
        self.ry += dt * self.vy;

    # Returns distance between two bodies
    def distanceTo(self, body):
        dx = self.rx - body.rx;
        dy = self.ry - body.ry;
        radical = math.sqrt(pow(dx,2) + pow(dy,2))
        if radical == 0:
            return 2e-100;
        else:
            return radical;
    
    # Reset force
    def resetForce(self):
        self.fx = 0.0;
        self.fy = 0.0;

    # Add force
    def addForce(self, b):
        #a = Body()
        EPS = 3e4; # softening parameter
        dx = b.rx - self.rx;
        dy = b.ry - self.ry;
        dist = math.sqrt(pow(dx,2) + pow(dy,2));
        F = (Body.G * self.mass * b.mass) / (dist * dist + EPS*EPS);
        self.fx += F * dx / dist;
        self.fy += F * dy / dist;

    # Verify if body in a quadrant
    def inQuad(self, quad):
        return quad.contains(self.rx, self.ry);

    # Combine two bodies
    def addBodies(self, b1, b2):
        bb = Body();
        bb.rx = (b1.rx*b1.mass + b2.rx*b2.mass) / (b1.mass + b2.mass);
        bb.ry = (b1.ry*b1.mass + b2.ry*b2.mass) / (b1.mass + b2.mass);
        bb.mass = (b1.mass + b2.mass);
        bb.color = b1.color;
        return bb;

    def __str__(self):
        return "r = (" + str(self.rx) + ", " + str(self.ry) + "), v = (" + str(self.vx) + ", " + str(self.vy) + "), m = " + str(self.mass);












