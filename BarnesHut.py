#!/usr/bin/python

import time
from visual import *
from math import *
from Quad import Quad
from Body import Body
from BHTree import BHTree

N = 20;
bodies = []; 
for i in xrange(N):
    x = Body()
    bodies.append(x)
q = Quad(0,0,2*1e18);

def signum(x):
    if x == 0:
        return 0.0;
    elif x > 0:
        return 1.0;
    else: 
        return -1.0;

def circlev(rx, ry):
    r2 = math.sqrt(rx*rx + ry*ry);
    numerator = (Body.G)*1e6*Body.SM;
    return math.sqrt(numerator/r2);

def exp(lamb):
    return -math.log(1 - random.uniform(0,1)) / lamb;

def startTheBodies():
    global N, bodies;
    R = 1e18; # Radius of Universe

    for i in range(1, N):
        # Initial Positions
        px = R * exp(-1.8) * (0.5 - random.uniform(0,1));
        py = R * exp(-1.8) * (0.5 - random.uniform(0,1));
        magv = circlev(px, py);

        absangle = math.atan(abs(px/py));
        thetav = math.pi/2 - absangle;
        phiv = random.uniform(0,1) * math.pi;
        vx = -1 * signum(py) * math.cos(thetav) * magv;
        vy = signum(px) * math.sin(thetav) * magv;

        if random.uniform(0,1) <= 0.5:
            vx = -vx;
            vy = -vy;

        mass = random.uniform(0,1) * Body.SM * 10 + 1e20;

        # Color the masses:
        red   = (mass * 254 / (Body.SM * 10 + 1e20)) / 255.0;
        blue  = 255 / 255.0;
        green = (mass * 254 / (Body.SM * 10 + 1e20)) / 255.0;
        colorm = (red, green, blue);

        bodies[i] = Body(px, py, vx, vy, mass, colorm, (red+green+blue)/6.0);

    # Central mass in:
    colorm = (1, 0, 0);
    bodies[0] = Body(0, 0, 0, 0, 1e6*Body.SM, colorm);


def addForces():
    global N, bodies, q;
    thetree = BHTree(q);
    # Add bodies to the tree
    for i in range(0, N):
        if bodies[i].inQuad(q):
            thetree.insert(bodies[i]);

    # Update forces, traveling recursively through the tree
    for i in range(0, N):
        bodies[i].resetForce();
        if bodies[i].inQuad(q):
            thetree.updateForce(bodies[i]);
            # Calculate the new positions on a time step dt 
            bodies[i].update(1e11);



scene.autoscale = True
scene.fullscreen = True
#scene.scale = (.1,.1,.1)
scaleFactor = 1e16
spheres = []

for i in range(0, N):
    startTheBodies()
    x = (bodies[i].rx)/scaleFactor;
    y = (bodies[i].ry)/scaleFactor;
    z = 0.0
    colorm = bodies[i].color
    #print str(x) + ", " + str(y)
    if i == 0:
        planet = sphere(radius = 1, pos = [x,y,z], color = colorm)
        planet.trail = curve(color = colorm)
        spheres.append(planet)
    else:
        print bodies[i].mass
        planet = sphere(radius = bodies[i].radius, pos = [x,y,z], color = colorm)
        planet.trail = curve(color = colorm)
        spheres.append(planet)


#for i in range(0, 1000):
while True:
    rate(30)
    #sleep(0.01)
    scene.autoscale = False
    
    addForces()

    for j in range(0, N):
        x = (bodies[j].rx)/scaleFactor;
        y = (bodies[j].ry)/scaleFactor;
        z = 0.0
        #print str(x) + ", " + str(y)
        spheres[j].pos = [x,y,z]
        spheres[j].trail.append(pos = spheres[j].pos)







