#!/usr/bin/python

from math import *

class Quad:

    # Constructor
    def __init__(self, xmid = 0, ymid = 0, length = 1):
        self.xmid = xmid;
        self.ymid = ymid;
        self.length = length;

    # Check if the current quadrant contains a point
    def contains(self, xmid, ymid):
        if (xmid <= (self.xmid+self.length/2.0)) and (xmid >= (self.xmid-self.length/2.0)) and (ymid <= (self.ymid+self.length/2.0)) and (ymid >= (self.ymid-self.length/2.0)):
            return True;
        else:
            return False;

    # Create Subdivisions:
    def NW(self):
        nwq = Quad(self.xmid - self.length/4.0, self.ymid + self.length/4.0, self.length/2.0);
        return nwq;

    def NE(self):
        nwq = Quad(self.xmid + self.length/4.0, self.ymid + self.length/4.0, self.length/2.0);
        return nwq;
    
    def SW(self):
        nwq = Quad(self.xmid - self.length/4.0, self.ymid - self.length/4.0, self.length/2.0);
        return nwq;
    
    def SE(self):
        nwq = Quad(self.xmid + self.length/4.0, self.ymid - self.length/4.0, self.length/2.0);
        return nwq;

    # Getter for length
    def getLength(self):
        return self.length;







